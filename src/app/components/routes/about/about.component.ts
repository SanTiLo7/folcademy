import { Component, OnInit } from '@angular/core';
import { CardUsersService } from '../../services/users/card-users.service';

interface Card {
  name: string,
  location: string,
  content: string | number,
}
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  providers: [CardUsersService]
})
export class AboutComponent  {

  users: any;

  constructor (
    private _userService: CardUsersService,
  ) {
    this.ObtenerUsuarios();
    }
    
  ObtenerUsuarios() {
    this._userService.RequestApi("user",6).subscribe (
      response => {
        this.users = response.data;
      }, 
      error => {
        console.log (error);
      }
    )
  }
  
  nombreT = '';
  locationT = '';
  contentT = '';
  
  person: Array<Card> = [
    {
      name: "Leandro",
      location: "Rawson",
      content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta esse delectus sequi sint laborum corporis necessitatibus a? Autem     accusantium iure unde aliquid sequi doloremque, maxime qui ex ad omnis",
    },
    {
      name: "María",
      location: "Santa Lucia",
      content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta esse delectus sequi sint laborum corporis necessitatibus a? Autem     accusantium iure unde aliquid sequi doloremque, maxime qui ex ad omnis",
    },
  ] 

  AgregarTestimonio() {
    const test: Card = {
      name: this.nombreT,
      location: this.locationT,
      content: this.contentT,
    }
    this.person.push(test);

    this.nombreT = '';
    this.locationT = '';
    this.contentT = '';
  }

}