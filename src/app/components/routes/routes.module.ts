import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CarouselModule } from 'ngx-owl-carousel-o';


import { HeroComponent } from './hero/hero.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { TestimonioComponent } from './about/testimonio/testimonio.component';
import { CommunityComponent } from './community/community.component';


@NgModule({
  declarations: [
    HeroComponent,
    ContactComponent,
    AboutComponent,
    TestimonioComponent,
    CommunityComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CarouselModule
  ],
  exports: [
    HeroComponent,
    ContactComponent
  ],
})
export class RoutesModule { }
