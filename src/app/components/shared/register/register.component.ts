import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  RegisterForm: FormGroup;

  user: any;
  error: boolean = false;

  constructor(

    private FromBuilder: FormBuilder, 
    private _authServices: AuthService,
    private router: Router) 
    
    {
    this.RegisterForm = this.FromBuilder.group(
      {
        email: ['', Validators.email],
        password: ['',[Validators.required, Validators.minLength(6)]],
      }
    )
    }

  ngOnInit(): void {
  }
  sendData () {
    this._authServices.register(this.RegisterForm.value).subscribe(
      response => {
        this.user = response;
        this.router.navigate(['/Community']);

      }, error => {
        console.log(error);
        this.error = true;
      }
    )
  }

}
