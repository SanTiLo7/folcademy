import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
    
  user: any;
  error: boolean = false;
  

  constructor(
    private FromBuilder: FormBuilder,
    private _authServices: AuthService,
    private router: Router) {
    this.LoginForm = this.FromBuilder.group(
      {
        email: ['', Validators.email],
        password: ['',[Validators.required, Validators.minLength(6)]],
      }
    )
   }

  ngOnInit(): void {
  }
  sendData () {
    this._authServices.login(this.LoginForm.value).subscribe(
      response => {
        this.user = response;
        this.router.navigate(['/Community']);
      }, error => {
        console.log(error);
        this.error = true;
      }
    )
  }
}
