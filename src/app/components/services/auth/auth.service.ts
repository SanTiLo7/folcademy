import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  ApiUrl: string = "https://identitytoolkit.googleapis.com/v1/";
  ApiKey: string = "AIzaSyDxnyQRxwQXA98UalhZYkzicQXKIY7knpI";


  constructor(private _http: HttpClient) {

  }

  register(user: any): Observable<any> {
    return this._http.post(this.ApiUrl+"accounts:signUp?key=AIzaSyDxnyQRxwQXA98UalhZYkzicQXKIY7knpI",user,{

    })
    
  }
  login(user: any): Observable<any> {
    return this._http.post(this.ApiUrl+"accounts:signInWithPassword?key=AIzaSyDxnyQRxwQXA98UalhZYkzicQXKIY7knpI",user,{

    })
    
  }
}
