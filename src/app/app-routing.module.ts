import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/routes/about/about.component';
import { CommunityComponent } from './components/routes/community/community.component';
import { ContactComponent } from './components/routes/contact/contact.component';
import { HeroComponent } from './components/routes/hero/hero.component';
import { LoginComponent } from './components/shared/login/login.component';
import { RegisterComponent } from './components/shared/register/register.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: HeroComponent
      },
      {
        path: 'About',
        component: AboutComponent
      },
      {
        path: 'Contact',
        component: ContactComponent
      },
      {
        path: 'Community',
        component: CommunityComponent
      },
      {
        path: 'Register',
        component: RegisterComponent
      },
      {
        path: 'Login',
        component: LoginComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }