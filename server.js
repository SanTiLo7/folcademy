const express = require('express');
const path = require ('path');
const app = express();

app.use(express.static('.dist/maskoto'));

app.get('/*', (reg, res) =>
    res.sendFile('index.html', {root: 'dist/maskoto/'}),
);

app.listen(process.env.PORT || 8080);